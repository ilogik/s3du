import sys
import boto3
from node import Node
from tqdm import tqdm
try:
    from config import AWS_ACCESS_KEY, AWS_SECRET_KEY
except ImportError:
    # try to let boto find the credentials
    AWS_ACCESS_KEY = None
    AWS_SECRET_KEY = None


s3_client = boto3.client(
    's3',
    aws_access_key_id=AWS_ACCESS_KEY,
    aws_secret_access_key=AWS_SECRET_KEY
)


bucket = sys.argv[1]
try:
    prefix = sys.argv[2]
except IndexError:
    prefix = ''

with tqdm() as pbar:
    node = Node(bucket, prefix, s3_client, pbar=pbar)
    for x in node.calculate():
        print(x)

print(node.bucket, node.prefix)
for k, v in node.data.items():
    print(k, v)
