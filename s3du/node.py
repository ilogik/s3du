from collections import namedtuple, defaultdict
from tqdm import tqdm

LsResult = namedtuple('LsResult', ['prefixes', 'files'])
FileInfo = namedtuple('FileInfo', ['key', 'storage_class', 'size',
                                   'last_modified'])


class StorageClassInfo:
    def __init__(self):
        self.file_count = 0
        self.size = 0
        self.ts_min = None
        self.ts_max = None
        self.ts_sum = 0

    def add(self, new):
        self.file_count += new.file_count
        self.size += new.size
        self.ts_sum += new.ts_sum
        if self.ts_min is None:
            self.ts_min = new.ts_min
            self.ts_max = new.ts_max
        else:
            self.ts_min = min(self.ts_min, new.ts_min)
            self.ts_max = max(self.ts_max, new.ts_max)

    def add_file(self, new_file: FileInfo):
        self.file_count += 1
        self.size += new_file.size
        if self.ts_min is None:
            self.ts_min = new_file.last_modified
            self.ts_max = new_file.last_modified
            self.ts_sum = new_file.last_modified
        else:
            self.ts_min = min(self.ts_min, new_file.last_modified)
            self.ts_max = max(self.ts_max, new_file.last_modified)
            self.ts_sum += new_file.last_modified

    def __repr__(self):
        return '<StorageClassInfo %d, %d, %d, %d, %d>' % (
            self.file_count,
            self.size,
            self.ts_min,
            self.ts_max,
            self.ts_sum
        )


class Node:
    def __init__(self, bucket, prefix, s3_client, pbar: tqdm):
        self.bucket = bucket
        self.prefix = prefix
        self.s3_client = s3_client
        self.files = []
        self.nodes = []
        self.pbar = pbar

        # one key/value for each storage class
        self.data = defaultdict(StorageClassInfo)

    @property
    def __ls(self):
        files = []
        prefixes = []
        first = True
        result = {}
        while first or 'NextContinuationToken' in result:
            if first:
                result = self.s3_client.list_objects_v2(
                    Bucket=self.bucket,
                    Prefix=self.prefix,
                    Delimiter='/'
                )
            else:
                result = self.s3_client.list_objects_v2(
                    Bucket=self.bucket,
                    Prefix=self.prefix,
                    Delimiter='/',
                    ContinuationToken=result['NextContinuationToken']
                )
            first = False
            prefixes.extend(
                [x['Prefix'] for x in result.get('CommonPrefixes', [])]
            )
            for content in result.get('Contents', []):
                files.append(FileInfo(
                    content['Key'],
                    content['StorageClass'],
                    content['Size'],
                    content['LastModified'].timestamp(),
                ))
        return LsResult(prefixes, files)

    def calculate(self):
        """ Recursively go through all the children
        """
        # prefixes, files = self.__ls
        prefixes, files = self.__ls
        self.pbar.update()

        self.files.extend(files)
        for f in files:
            self.data[f.storage_class].add_file(f)

        for prefix in prefixes:
            prefix_node = Node(self.bucket, prefix, self.s3_client, self.pbar)
            prefix_data = prefix_node.calculate()
            for k, v in prefix_data.items():
                self.data[k].add(v)

        return self.data

    def __repr__(self):
        return 's3://%s%s' % (self.bucket, self.prefix)
